#ifndef OOPSH_RETO_H
#define OOPSH_RETO_H

#define SHELL_BUFF_SIZE 1024

const char *getUserName(void);
int obtener_mandatos(char ****argvs, char **file_in, char **file_out, char **file_err, int *bg, int estado);

#endif //OOPSH_RETO_H
