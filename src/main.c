/*
 * Solución de la Práctica: Intérprete de mandatos sencillo
 */
#include "reto.h"
#include "y.tab.c"
#include "lex.yy.c"
#include <pwd.h>
#include <sys/wait.h>
#include <fcntl.h>

const int IN  = 0;
const int OUT = 1;
const int ERR = 2;

const char *getUserName()
{
    uid_t uid = geteuid();
    struct passwd *pw = getpwuid(uid);
    if (pw) { return pw->pw_name; }

    return "";
}

int obtener_mandatos(char ****argvs, char **file_in,
                     char **file_out, char **file_err,
					 int *bg, int estado)
{
	printf("{%s}[%d]$ ", getUserName(), estado);
	return obtener_mandato(argvs, file_in, file_out, file_err, bg);
}

int main(void)
{
	char ***argvs;
	char **argv;
	int mandatos;
	char *file_in;
	char *file_out;
	char *file_err;
	int bg;
	int i, j;
	pid_t cpid;
	pid_t epid;
	int estado = 0;

	setbuf(stdout, NULL);			
	setbuf(stdin, NULL);


	while ((mandatos = obtener_mandatos(&argvs, &file_in, &file_out, &file_err, &bg, estado)) != EOF) {
		if (mandatos < 1)
			continue;

		int pipes[mandatos - 1][2];
		for (int k=0; k < mandatos - 1; k++) {
			pipe(pipes[k]);
		}

		for (i = 0; i < mandatos; i++) {
			argv = argvs[i];	/* mandato simple */

			/* hijo */
			if ((cpid = fork()) == 0) {

				/* OUT -> IN */
				if (i > 0) {
//					printf("[%d] RD pipe open.\n", getpid());
					close(pipes[i - 1][OUT]);
					close(STDIN_FILENO);
					dup2(pipes[i - 1][IN], IN);
					close(pipes[i - 1][IN]);
//					printf("[%d] RD pipe closed.\n", getpid());
				}

				if (i + 1 < mandatos) {
//					printf("[%d] WR pipe open.\n", getpid());
					close(pipes[i][IN]);
					close(STDOUT_FILENO);
					dup2(pipes[i][OUT], OUT);
					close(pipes[i][OUT]);
//					printf("[%d] WR pipe closed.\n", getpid());
				}

				/* redireccion de IN, OUT, ERR */
				if (file_in != NULL) {
					int ifd = open(file_in, O_RDONLY, S_IRUSR);
					dup2(ifd, IN);
					close(ifd);
				}

				if (file_out != NULL) {
					int ofd = open(file_out, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
					dup2(ofd, OUT);
					close(ofd);
				}
				if (file_err != NULL) {
					int efd = open(file_err, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
					dup2(efd, ERR);
					close(efd);
				}

				/* ejecucion del mandato */
				exit(execvp(argv[0], argv));
			}
		}
		/* padre */
		if (!bg) {
//			printf("[i]\tEsperando a %d...\n", cpid);
			while ((epid = wait(&estado)) > cpid) {
//				printf("[i]\tTerminado %d.\n", epid);
			}
		} else {
			printf("[%d]\n", cpid);
		}
	}
	return 0;
}
