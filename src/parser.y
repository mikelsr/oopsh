%{
/*-
 * parse.y
 * 
 */

static char *** argvv;
static int argvc;
static int argc;
static char *f_in;
static char *f_out;
static char *f_err;
static int bg;

static void pipeline();
static void freevv(char ***ppp);
static void command(char *arg);
%}


%start msh
%token TXT
%right '|'
%union { char * txt; }
%type <txt> TXT

%%
msh	:			{ return(0); }
	| line fin		{ return(argvc+1); }
	| error fin		{ yyerrok; return(-1); }
	;

line	:
	| pipel redir backg
	;

pipel	: comma			{ pipeline(); }
	| pipel '|' comma	{ pipeline(); }
	;

comma	: TXT			{ command($1); }
	| comma TXT		{ command($2); }
	;

redir	:
	| '<' TXT redir		{ if(f_in) YYERROR; f_in = $2; }
	| '>' TXT redir		{ if(f_out) YYERROR; f_out = $2; }
	| '>' '&' TXT redir	{ if(f_err) YYERROR; f_err = $3; }
	;

backg	:
	| '&'			{ bg = 1; }
	;

fin	: '\n'
	;
%%

#include <stddef.h>			
#include <stdlib.h>		
#include <stdio.h>	

#define zfree(p) { if(p) { free(p); p = NULL; } }
#define zfreevv(ppp) { if(ppp) { freevv(ppp); ppp = NULL; } }

static void freevv(char ***ppp)
{
	char **pp;
	for (; ppp && *ppp; ppp++)
		for (pp = *ppp; pp && *pp; pp++)
			zfree(*pp);
}

static void pipeline()
{
	argvc++;
}

#define argv argvv[argvc]

static void command(char *arg)
{
	if (!argvv) {
		argvc = 0;
		argvv = calloc(1, sizeof(char **));
	}
	if (!argv) {
		argvv = realloc(argvv, (argvc + 2) * sizeof(char **));
		argvv[argvc + 1] = NULL;
		argc = 0;
		argv = calloc(1, sizeof(char *));
	}
	argv = realloc(argv, (argc + 2) * sizeof(char *));
	argv[argc++] = arg;
	argv[argc] = NULL;
}

/* 
 * Funci�n: obtener_mandato
 * Devuelve:
 *   -2, en caso de error
 *    -1 (EOF, at end of file on input
 *   >0, number of commands in the pipeline + 1
 */
int obtener_mandato(char ****argvs, char **file_in, char **file_out, char **file_err, int *bgp)
/* argvs is reference of a NULL terminated array of argvs */
/* filep is reference to an array of 3 char* */
/* bgp is reference to an integer */
{
	int ret;

    /* He puesto el prompt en main.c */

	zfreevv(argvv);
	argvc = 0;
	zfree(f_in);
	zfree(f_out);
	zfree(f_err);
	bg = 0;

	ret = yyparse();

	*argvs = argvv;


	*file_in = f_in;
	*file_out = f_out;
	*file_err = f_err;
	*bgp = bg;


	return ret-1;
}
